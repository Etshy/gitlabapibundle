<?php declare(strict_types=1);

namespace Etshy\Gitlabapibundle\DependencyInjection;

use Gitlab\Client;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpClient\Psr18Client;

class EtshyGitlabApiExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->addClients($config['clients'], $container);
    }

    private function addClients(array $clients, ContainerBuilder $container): void
    {

        foreach ($clients as $name => $client) {
            $this->createClient(
                $name,
                $client['url'],
                $client['token'],
                $client['auth_method'],
                $client['sudo'],
                $client['alias'],
                $client['http_client'],
                $container
            );
        }

        if (array_key_exists('default', $clients)) {
            $this->setDefaultClient('default', $container);
        } else {
            reset($clients);
            $this->setDefaultClient(key($clients), $container);
        }
    }

    private function createClient(string $name, ?string $url, ?string $token, ?string $auth_method, ?string $sudo, ?string $alias, ?string $http_client, ContainerBuilder $container): void
    {
        if ($http_client) {
            //Build the Gitlab Client with $http_client
            $container->setAlias(sprintf('etshy_gitlab_api.http_client.%s', $name), $http_client);

            $definition = new Definition();
            $definition->setClass(Client::class);
            $definition->addArgument(new Reference($http_client));
        } else {
            //Use Symfony PSR18Client by default
            $psr18Client = new Definition(Psr18Client::class, [new Reference('http_client')]);

            $definition = new Definition();
            $definition->setClass(Client::class);
            $definition->addArgument($psr18Client);
        }

        $definition->setFactory([Client::class, 'createWithHttpClient']);

        if ($url) {
            //setUrl only if set. by default, it uses gitlab.com
            $definition->addMethodCall('setUrl', [$url]);
        }

        if ($token) {
            //Authenticate only if there is a token. without token only public api on public repositories can be called
            $definition->addMethodCall('authenticate', [
                $token,
                $auth_method,
                $sudo,
            ]);
        }

        // Add Service to Container
        $container->setDefinition(
            sprintf('etshy_gitlab_api.client.%s', $name),
            $definition
        );

        // If alias option is set, create a new alias
        if (null !== $alias) {
            $container->setAlias($alias, sprintf('etshy_gitlab_api.client.%s', $name));
        }
    }

    private function setDefaultClient($name, ContainerBuilder $container): void
    {
        if ($name !== 'default') {
            $container->setAlias('etshy_gitlab_api.client.default', sprintf('etshy_gitlab_api.client.%s', $name));
        }
        $container->setAlias(Client::class, 'etshy_gitlab_api.client.default');
    }
}
