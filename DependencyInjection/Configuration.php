<?php declare(strict_types=1);

namespace Etshy\Gitlabapibundle\DependencyInjection;

use Gitlab\Client as GitlabClient;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('etshy_gitlab_api');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode("clients")
                    ->useAttributeAsKey('name') // keep the name of the client
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('token')
                                ->defaultNull()
                            ->end()
                            ->scalarNode('url')
                                ->defaultNull()
                            ->end()
                            ->enumNode('auth_method')
                                ->defaultValue(GitlabClient::AUTH_HTTP_TOKEN)
                                ->values(array(GitlabClient::AUTH_HTTP_TOKEN, GitlabClient::AUTH_OAUTH_TOKEN))
                                ->info('Possible values are ' .  GitlabClient::AUTH_HTTP_TOKEN . ' and ' . GitlabClient::AUTH_OAUTH_TOKEN)
                            ->end()
                            ->scalarNode('sudo')
                                ->defaultNull()
                            ->end()
                            ->scalarNode('alias')
                                ->defaultValue(null)
                            ->end()
                            ->scalarNode('http_client')
                                ->defaultValue(null)
                            ->end()
                        ->end()//children
                    ->end()//arrayPrototype
                ->end()//array_node clients
            ->end()//children
        ;//rootNode

        return $treeBuilder;
    }
}
