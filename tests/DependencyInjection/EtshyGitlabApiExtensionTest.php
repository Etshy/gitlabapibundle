<?php declare(strict_types=1);

namespace Etshy\Gitlabapibundle\Test\DependencyInjection;

use Etshy\Gitlabapibundle\DependencyInjection\EtshyGitlabApiExtension;
use Exception;
use Gitlab\Client;
use Http\Client\HttpClient;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpClient\MockHttpClient;

class EtshyGitlabApiExtensionTest extends TestCase
{
    private ContainerBuilder $container;
    private EtshyGitlabApiExtension $extension;

    protected function setUp(): void
    {
        $this->container = new ContainerBuilder();
        $this->container->setDefinition('http_client', new Definition(MockHttpClient::class));
        $this->extension = new EtshyGitlabApiExtension();
    }

    public function testCreateClientsWithDefault(): void
    {
        $configs = [
            'etshy_gitlab_api' => [
                'clients' => [
                    'firstclient' => [],
                    'default' => [
                        'token' => '12345',
                        'url' => 'http://example.com/api/v3/',
                    ],
                ],
            ],
        ];

        $this->extension->load($configs, $this->container);

        $this->assertInstanceOf(Client::class, $this->container->get('etshy_gitlab_api.client.default'));
        $this->assertInstanceOf(Client::class, $this->container->get('etshy_gitlab_api.client.firstclient'));
        $this->assertNotSame($this->container->get('etshy_gitlab_api.client.default'), $this->container->get('etshy_gitlab_api.client.firstclient'));
    }

    public function testCreateClients(): void
    {
        $configs = [
            'etshy_gitlab_api' => [
                'clients' => [
                    'firstclient' => [],
                    'secondclient' => [
                        'token' => '12345',
                        'url' => 'http://example.com/api/v3/',
                    ],
                ],
            ],
        ];

        $this->extension->load($configs, $this->container);

        $this->assertInstanceOf(Client::class, $this->container->get('etshy_gitlab_api.client.secondclient'));
        $this->assertInstanceOf(Client::class, $this->container->get('etshy_gitlab_api.client.firstclient'));
        $this->assertSame($this->container->get('etshy_gitlab_api.client.default'), $this->container->get('etshy_gitlab_api.client.firstclient'));
    }

    public function testWrongAuthMethod(): void
    {
        $this->expectException(InvalidConfigurationException::class);

        $configs = [
            'etshy_gitlab_api' => [
                'clients' => [
                    'firstclient' => [
                        'auth_method' => 'abcd',
                    ],
                ],
            ],
        ];

        $this->extension->load($configs, $this->container);
    }

    public function testClientAlias(): void
    {
        $configs = [
            'etshy_gitlab_api' => [
                'clients' => [
                    'firstclient' => [
                        'alias' => 'firstalias',
                    ],
                    'secondclient' => [
                        'token' => '12345',
                        'url' => 'http://example.com/api/v3/',
                    ],
                ],
            ],
        ];

        $this->extension->load($configs, $this->container);

        $this->assertTrue($this->container->has('etshy_gitlab_api.client.default'));
        $this->assertTrue($this->container->has('firstalias'));
        $this->assertTrue($this->container->has('etshy_gitlab_api.client.firstclient'));
        $this->assertTrue($this->container->has('etshy_gitlab_api.client.secondclient'));
        $this->assertSame($this->container->get('etshy_gitlab_api.client.default'), $this->container->get('firstalias'));
        $this->assertSame($this->container->get('firstalias'), $this->container->get('etshy_gitlab_api.client.firstclient'));
        $this->assertNotSame($this->container->get('etshy_gitlab_api.client.firstclient'), $this->container->get('etshy_gitlab_api.client.secondclient'));
    }

    /**
     * Test if the getHttpClient method of Gitlab\Client return a Http\Client\HttpClient
     * @return void
     * @throws Exception
     */
    public function testHttpClients(): void
    {
        $configs = [
            'etshy_gitlab_api' => [
                'clients' => [
                    'firstclient' => [],
                    'secondclient' => [
                        'token' => '12345',
                        'url' => 'http://example.com/api/v3/',
                        'http_client' => 'http.client',
                    ],
                ],
            ],
        ];

        $httpClient = $this->createMock(HttpClient::class);
        $this->container->setDefinition('http.client', new Definition(HttpClient::class));
        $this->container->set('http.client', $httpClient);

        $this->extension->load($configs, $this->container);

        $firstClient = $this->container->get('etshy_gitlab_api.client.firstclient');
        $secondClient = $this->container->get('etshy_gitlab_api.client.secondclient');

        $this->assertInstanceOf(
            HttpClient::class,
            $firstClient->getHttpClient()
        );

        $this->assertInstanceOf(
            HttpClient::class,
            $secondClient->getHttpClient()
        );
    }
}
