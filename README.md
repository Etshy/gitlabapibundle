# EtshyGitlabApiBundle

This Bundle integrates the [GitLab PHP API Client](https://github.com/GitLabPHP/Client) into your Symfony Project.

Very inspired by [Zeichen32GitLabApiBundle](https://github.com/Zeichen32/GitLabApiBundle).

I made this Bundle to be able to have unauthenticated Gitlab Api client (to hit public endpoint)

## 1. Installation

The most recommended way is using [composer](https://getcomposer.org/)

> composer require etshy/gitlabapi-bundle

If your application use `Symfony/flex` you're done with the installation

Otherwise you'll have to do the following

```php
// config/bundles.php
// ...
return [
    ...
    Etshy\Gitlabapibundle\EtshyGitlabApiBundle::class => ['all' => true],
    ...
];
```

## 2. Configure

```yml
etshy_gitlab_api:
  clients:
    client_1: # This is used as name to complete default alias syntax : etshy_gitlab_api.client.client_1
      # All options below are optional
      token: api-token # if not present, only public api will work
      url: https://gitlab.com # by default to gitlab.com. could be used to access your self-hosted gitlab
      auth_method: http_token # http_token or oauth_token
      sudo: ~ # use the sudo param of the GitLab PHP API Client, should be a user's ID or username to impersonate 
      # (more details about sudo on the API doc : https://docs.gitlab.com/ee/api/#sudo)
      alias: service_alias # Symfony service custom alias
      http_client: http_client_service_alias # the symfony service alias for a HttpClient to use. by Default we use Symfony Psr18Client
```

This bundle will make a default client with either the first client configured or the one named 'default'

## 3. Usage

You should now be able to use the default client using type Hinting

````php
use Gitlab\Client;

class MyClass
{
    public function __construct(Client $client)
}
````

Through this $client, you'll be able to use the [GitLab PHP API Client](https://github.com/GitLabPHP/Client) methods
```php
$client->api('issues')->all($project_id);
```
or instantiate other GitLab PHP API Client Classes like :
`````php
$pager = new Gitlab\ResultPager($client);
`````

If you want to instantiate another client, you'll need to specify it in the `services.yml`
```yml
services:
    MyClass:
        arguments: { $client: '@etshy_gitlab_api.client.client_1' }
```

